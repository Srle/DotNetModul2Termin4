﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knjizara2.Models;

namespace Knjizara2.Controllers
{
    public class BookstoreController : Controller
    {
        public static List<Book> bookList1 = new List<Book>()
              {
                new Book(){ Id = 1, Name = "Knjiga 1", Price = 100.5, Genre = "Science", Deleted=false },
                new Book(){ Id = 2, Name = "Knjiga 2", Price = 250.8, Genre = "Comedy", Deleted=true },
                new Book(){ Id = 3, Name = "Knjiga 3", Price = 524, Genre = "Science", Deleted=false },
                new Book(){ Id = 4, Name = "Knjiga 4", Price = 325.8, Genre = "Comedy", Deleted=true }
            };
        public Bookstore bookStore1 = new Bookstore(1, "Knjizara1", bookList1);


        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddBook(string NameOfBook, double PriceOfBook, string GenreOfBook)
        {
            int id = bookList1.Count + 1;
            Book bk = new Book(id, NameOfBook, PriceOfBook, GenreOfBook, false);
            bookList1.Add(bk);

            return RedirectToAction("List"); 
        }

        public ActionResult List()
        {
            return View(bookList1);
        }

      
        public ActionResult Delete(int id)
        {
            for (int i = 0; i < bookList1.Count; i++)
            {
                if (bookList1[i].Id == id)
                {
                    bookList1[i].Deleted = true;
                }
            }

            return RedirectToAction("Deleted");
        }

        public ActionResult Deleted()
        {
            return View(bookList1);
        }




    }
}